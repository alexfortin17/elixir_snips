defmodule TestList do
  def empty?([]), do: true
  def empty?([_|_]), do: false
end

defmodule ListHelper do
  def sum([]) do
    0
  end

  def sum([head | tail]) do
    head + sum(tail)
  end
end
