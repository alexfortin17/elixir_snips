defmodule StreamTest do
  def printSqrts(ints) do
    Stream.filter(ints, &(is_number(&1) and &1 > 0))
    |> Stream.map(&{&1, :math.sqrt(&1)})
    |> Enum.map(fn {x,y} -> IO.puts("The square root of #{x} is #{y}") end)
  end

  # lazily gets lines of a file as a stream, evaluating them one at a time
  # gets rid of the line return and add to final list if more than 80 chars on line
  def large_lines!(path) do
    File.stream!(path)
    |> Stream.map(&String.replace(&1, "\n", ""))
    |> Enum.filter(&(String.length(&1) > 80))
  end

  # Lazily evaluating then longest line of a file (recursively through reduce)
  def longest_line_length(path) do
    File.stream!(path)
    |> Enum.reduce(0, fn elem, acc ->
      if String.length(elem) > acc do
        String.length(elem)
      else
        acc
      end
    end)
  end

end
