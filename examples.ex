defmodule Geometry do
 def rectangle_area(a, b) do
 a * b
 end
end

defmodule Rectangle do
 def area(a), do: area(a, a) end

 def area(a, b), do: a * b end
end

defmodule Calculator do
  def sum(a, b \\0) do
    a + b
  end
end
