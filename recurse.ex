defmodule NaturalNums do
  def print(1) do
    IO.puts(1)
  end

  def print(n) do
    print(n-1)
    IO.puts(n)
  end

end

defmodule ListHelper do
  def sum([]) do
    0
  end

  def sum([head|tail]) do
    head + sum(tail)
  end

  # tail recursive version of sum (stack safe)
  def altsum(list) do
    do_sum(0, list)
  end

  defp do_sum(current_sum, []) do
    current_sum
  end

  defp do_sum(current_sum, [h|t]) do
    new_sum = current_sum + h
    do_sum(new_sum, t)
  end

end
